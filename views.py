#
# Copyright (C) 2016-2022 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
import json
import os
from six.moves import urllib

import insult

from django.http import HttpResponse, HttpResponseBadRequest
from django.conf import settings
from django.template import loader
from django.views.generic import View
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.views.decorators.csrf import csrf_exempt


class InsulterError(Exception):
    pass


def setting(name, default):
    return getattr(settings, "INSULT_"+name.upper(), default)


class FormatBase(View):
    insulter_dir = setting("WORD_PATH", None)
    default_language = setting("LANGUAGE", "en")
    _default_template_tail = (
        "as <adjective> as <article target=adj1> "
        "<adjective min=1 max=3 id=adj1> <amount> of "
        "<adjective min=1 max=3> <animal> <animal_part>"
    )
    default_template = setting("TEMPLATE", "You are " + _default_template_tail)
    ## TODO plural
    template_third_person = setting("TEMPLATE_THIRD_PERSON", "<who> is " + _default_template_tail)
    template_third_person_plural = setting("TEMPLATE_THIRD_PERSON_PLURALE", "<who> are " + _default_template_tail)

    default_template_corporate = setting("CORPORATE_TEMPLATE", "<adverb> <verb> <adjective min=1 max=3> <noun>")
    template_corporate_third_person = setting(
        "CORPORATE_TEMPLATE_THIRD_PERSON",
        "<who> <adverb> <verb id=verb><verb_3rd target=verb> <adjective min=1 max=3> <noun>"
    )
    template_corporate_person = setting(
        "CORPORATE_TEMPLATE_PERSON",
        "<who> " + default_template_corporate
    )

    max_repetitions = setting("MAX_REPETITIONS", 5)
    allow_override_language = True
    allow_override_template = True

    def valid_language(self, lang):
        return all(c.isalpha() or c == "_" for c in lang)

    def insulter(self, lang):
        if not self.insulter_dir:
            ## \todo Provide this as a constant in libinsult
            self.insulter_dir = os.path.join(
                os.path.dirname(os.path.dirname(insult.__file__)),
                "share",
                "libinsult",
                "word_lists"
            )

        path = os.path.join(self.insulter_dir, lang)

        if not self.valid_language(lang) or not os.path.isdir(path):
            raise InsulterError("Invalid language")

        insulter = insult.Insulter()
        insulter.load_directory(path)
        insulter.set_max(self.max_repetitions)
        return insulter

    def _get_template(self, args, default, third_person, other):
        if not self.allow_override_template:
            return default

        if "who" in args:
            if "plural" in args:
                return other.replace("<who>", args["who"])
            else:
                return third_person.replace("<who>", args["who"])
        else:
            return args.get("template", self.default)

    def normalize_request_args(self, args):
        lang = args.get("lang", self.default_language) if self.allow_override_language else self.default_language

        if "corporate" in lang:
            template = self._get_template(args, self.default_template_corporate, self.template_corporate_third_person, self.template_corporate_person)
        elif self.allow_override_template:
            template = self._get_template(args, self.default_template, self.template_third_person, self.template_third_person_plural)

        return {
            "lang": lang,
            "template": template
        }

    def get(self, request):
        return self.format_response(request, self.handle(request.GET))

    def format_response(self, request, response):
        response["X-Insult-Url"] = request.get_full_path()
        return response

    def handle(self, request_args):
        try:
            args = self.normalize_request_args(request_args)
            insulter = self.insulter(args["lang"])
            insult = self.format(insulter.format(args["template"]), args)
            return HttpResponse(insult, content_type=self.content_type)
        except InsulterError as err:
            return self.bad_request(self.error(str(err)))
        except Exception:
            if settings.DEBUG:
                raise
            return self.bad_request(self.error("Unexpected error"))

    def bad_request(self, contents):
        return HttpResponseBadRequest(contents, content_type=self.content_type)

    def error(self, error):
        return HttpResponseBadRequest()

    def format(self, insult, args):
        raise NotImplementedError()


class JsonFormat(FormatBase):
    content_type = "text/json"
    name = "json"

    def format(self, insult, args):
        return json.dumps({
            "error": False,
            "args": args,
            "insult": insult,
        })

    def error(self, error):
        return json.dumps({
            "error": True,
            "error_message": error
        })


class TextFormat(FormatBase):
    content_type = "text/plain"
    name = "txt"

    def format(self, insult, args):
        return insult

    def error(self, error):
        return ""


class HtmlFormat(FormatBase):
    content_type = "text/html"
    name = "html"

    def render(self, template, **kwargs):
        return loader.get_template(template).render(kwargs)

    def format(self, insult, args):
        return self.render("django_insult/format.html", insult=insult)

    def error(self, error):
        return self.render(
            "django_insult/error.html",
            message=error,
            error_code=400
        )


output_formats = [TextFormat(), JsonFormat(), HtmlFormat()]


class FormatDispatcher(FormatBase):
    def get(self, request, format, **kwargs):
        params = request.GET.copy()
        params.update(kwargs)
        for format_object in output_formats:
            if format_object.name == format:
                self.format_object = format_object
                return self.format_response(request, self.handle(params))
        return self.format_response(request, HttpResponseNotFound())

    def format(self, insult, args):
        return self.format_object.format(insult, args)

    def error(self, insult):
        return self.format_object.error(insult)

    @property
    def content_type(self):
        return self.format_object.content_type

    @property
    def name(self):
        return self.format_object.name


def help(request):
    return render(request, "django_insult/api_help.html", {
        "formats": output_formats,
        "default_language": FormatBase.default_language,
        "default_template": FormatBase.default_template,
    })


def get_url(args):
    lang = args.pop("lang", None)
    format = args.pop("format", "txt")

    if "who" in args and not args["who"]:
        args.pop("who")

    if "template" in args and not args["template"]:
        args.pop("template")

    if lang:
        url = reverse("insult:api_lang", kwargs=({
            "lang": lang,
            "format": format
        }))
    else:
        url = reverse("insult:api", kwargs=({
            "format": format
        }))

    if args:
        url += "?" + urllib.parse.urlencode(args)

    return url


@csrf_exempt
def explore(request):
    if request.method == "POST":
        return redirect(get_url(request.POST.dict()))

    return render(request, "django_insult/api_explore.html", {
        "languages": [
            lang
            for lang in os.listdir(FormatBase.insulter_dir)
            if os.path.isdir(os.path.join(FormatBase.insulter_dir, lang))
        ],
        "default_language": FormatBase.default_language,
        "formats": output_formats,
    })
