#
# Copyright (C) 2016-2022 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
from ..views import FormatBase

from django import template
from django.conf import settings

register = template.Library()


@register.simple_tag
def insult(template="", lang="en"):
    """!
    Template tag for insults
    """
    try:
        if not template:
            template = FormatBase.default_template
        return FormatBase().insulter(lang).format(template)
    except Exception:
        if settings.DEBUG:
            raise
        return ""

