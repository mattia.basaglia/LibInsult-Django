LibInsult Django
================

This repository contains a Django app implementing the insult API.


Intallation
-----------

Make sure the insult library is installed and in PYTHONPATH
(you can install it with pip).

Download the insult data, and set INSULT_WORD_PATH in the django settings.


License
-------

GPLv3+, see COPYING


Sources
-------

See http://insult.mattbas.org


Author
------

Mattia Basaglia <mattia.basaglia@gmail.com>
